from flask import Flask, render_template, request, redirect, url_for
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.preprocessing import StandardScaler
import pickle
from sklearn.metrics import mean_squared_error
import hashlib # Для хеширования паролей


app = Flask(__name__)
app.secret_key = 'd87s78dsfhsdufiohs7' 


# Загрузка модели
model = pickle.load(open("model.pkl","rb"))


# База данных пользователей 
users = {
    "user1": hashlib.sha256("password1".encode()).hexdigest(), # Хешированный пароль
    "user2": hashlib.sha256("password2".encode()).hexdigest() # Хешированный пароль
}


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        username = request.form['username']
        password = hashlib.sha256(request.form['password'].encode()).hexdigest()
        if username in users and users[username] == password:
            return redirect(url_for('prediction'))
        else:
            return "Неверный логин или пароль"
    return render_template('index.html')


feature_order = ['building_type', 'level', 'levels', 'rooms', 'area', 'kitchen_area', 'object_type', 'kremlin_dist.km', 'level_to_levels', 'area_to_rooms', 'side', 'year', 'month']

@app.route('/prediction', methods=['GET', 'POST'])
def prediction():
    if request.method == 'POST':
        try:
            # Получаем данные от пользователя
            input_data = {
                'building_type': int(request.form['building_type']),
                'level': int(request.form['level']),
                'levels': int(request.form['levels']),
                'rooms': int(request.form['rooms']),
                'area': float(request.form['area']),
                'kitchen_area': float(request.form['kitchen_area']),
                'object_type': int(request.form['object_type']),
                'kremlin_dist.km': float(request.form['kremlin_dist.km']),
                'side': int(request.form['side']),
                'year': int(request.form['year']),
                'month': int(request.form['month'])
            }

            #проверка диапазонов
            if not (2018 <= input_data['year'] <= 2021):
                raise ValueError("Год должен быть в диапазоне 2018-2021")
            if not (1 <= input_data['month'] <= 12):
                raise ValueError("Месяц должен быть в диапазоне 1-12")
            if input_data['rooms'] == 0 and (input_data['area'] - input_data['kitchen_area'])< 0:
                raise ValueError("Площадь комнаты не может быть меньше 0")
            if input_data['levels'] == 0:
                raise ValueError("Этажность здания не может быть равна 0")


            input_data['level_to_levels'] = input_data['level'] / input_data['levels']
            if input_data['rooms'] > 0:
                input_data['area_to_rooms'] = (input_data['area'] - input_data['kitchen_area']) / input_data['rooms']
            else:
                input_data['area_to_rooms'] = input_data['area']

            # Создаем DataFrame в нужном порядке
            df = pd.DataFrame([input_data])[feature_order]

            # Предсказываем цену
            predicted_price = model.predict(df)[0]
            return render_template('result.html', price=predicted_price)
        except ValueError as e:
            return render_template('prediction.html', error_message=f"Ошибка ввода данных: {e}") 
        except KeyError as e:
            return render_template('prediction.html', error_message=f"Ошибка: Отсутствует ключ {e}")
        except Exception as e:
            return render_template('prediction.html', error_message=f"Произошла неизвестная ошибка: {e}")
    return render_template('prediction.html')


if __name__ == '__main__':
    app.run(debug=True)
